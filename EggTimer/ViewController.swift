//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation



class ViewController: UIViewController {

    let eggTimes = ["Soft":3.0, "Medium":4.0,"Hard":7.0]
    var totalTime = 0.0
    var secondsPassed = 0.0
    var timer = Timer()
    var player: AVAudioPlayer!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBAction func hardnessSelected(_ sender: UIButton) {
        timer.invalidate()
        
        let hardness = sender.currentTitle!
        totalTime = eggTimes[hardness]!
        
        progressBar.progress = 0.0
        secondsPassed = 0
        titleLabel.text = hardness

        titleLabel.text = "\(hardness)."

        
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
        
    @objc func updateTimer(){
            
        if secondsPassed < totalTime {
            progressBar.progress = Float(secondsPassed)/Float(totalTime)
            secondsPassed += 0.01


        } else {
            timer.invalidate()
            progressBar.progress = 1.0
            titleLabel.text = "Done."
            let url = Bundle.main.url(forResource: "alarm_sound_prime", withExtension: "mp3")
                    player = try! AVAudioPlayer(contentsOf: url!)
                    player.play()

        }
        
    }
    
}
